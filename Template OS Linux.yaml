zabbix_export:
  version: '6.4'
  template_groups:
    - uuid: 7df96b18c230490a9a0a9e2307226338
      name: Templates
  templates:
    - uuid: edc9f0a5a5d54797bf68e37364743831
      template: 'Template OS Linux'
      name: 'Template OS Linux'
      templates:
        - name: 'Template App Zabbix Agent'
      groups:
        - name: Templates
      items:
        - uuid: 6f0d30b413f746f1857c142a7d4ab2dc
          name: 'Maximum number of opened files'
          key: kernel.maxfiles
          delay: 1h
          history: 1w
          description: 'It could be increased by using sysctrl utility or modifying file /etc/sysctl.conf.'
          request_method: POST
          tags:
            - tag: Application
              value: OS
          triggers:
            - uuid: d56828a661f9498a99cfb645cd8f30fa
              expression: 'last(/Template OS Linux/kernel.maxfiles)<1024'
              name: 'Configured max number of opened files is too low on {HOST.NAME}'
              priority: INFO
        - uuid: 679313e0039c461a8f34badfb9390545
          name: 'Maximum number of processes'
          key: kernel.maxproc
          delay: 1h
          history: 1w
          description: 'It could be increased by using sysctrl utility or modifying file /etc/sysctl.conf.'
          request_method: POST
          tags:
            - tag: Application
              value: OS
          triggers:
            - uuid: b9c6644db41a49fbaf84b22c30f72dc4
              expression: 'last(/Template OS Linux/kernel.maxproc)<256'
              name: 'Configured max number of processes is too low on {HOST.NAME}'
              priority: INFO
        - uuid: 2bb3314953be4cd290cce97926ad6276
          name: 'Number of running processes'
          key: 'proc.num[,,run]'
          history: 1w
          description: 'Number of processes in running state.'
          request_method: POST
          tags:
            - tag: Application
              value: Processes
          triggers:
            - uuid: aae64f9f597a437da98726646f970f9b
              expression: 'avg(/Template OS Linux/proc.num[,,run],5m)>30'
              name: 'Too many processes running on {HOST.NAME}'
              priority: WARNING
        - uuid: 160ccce412104e75be08bcedd30c8d29
          name: 'Number of processes'
          key: 'proc.num[]'
          history: 1w
          description: 'Total number of processes in any state.'
          request_method: POST
          tags:
            - tag: Application
              value: Processes
          triggers:
            - uuid: 4da0d3f0571f4ea6be35fb5224e319c4
              expression: 'avg(/Template OS Linux/proc.num[],5m)>300'
              name: 'Too many processes on {HOST.NAME}'
              priority: WARNING
        - uuid: dbe46edd97724bb6bcad126dcfc21098
          name: 'Host boot time'
          key: system.boottime
          delay: 10m
          history: 1w
          units: unixtime
          request_method: POST
          tags:
            - tag: Application
              value: General
            - tag: Application
              value: OS
        - uuid: 4cead1ac32c142958f93fe4c1b17b0f8
          name: 'Interrupts per second'
          key: system.cpu.intr
          history: 1w
          units: ips
          preprocessing:
            - type: CHANGE_PER_SECOND
              parameters:
                - ''
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 1cd6d414685a4ccbacf2b0d35b15c815
          name: 'Processor load (1 min average per core)'
          key: 'system.cpu.load[percpu,avg1]'
          history: 1w
          value_type: FLOAT
          description: 'The processor load is calculated as system CPU load divided by number of CPU cores.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
          triggers:
            - uuid: 7b8def10fd044d9e881eb0611dcb8fa0
              expression: 'avg(/Template OS Linux/system.cpu.load[percpu,avg1],5m)>5'
              name: 'Processor load is too high on {HOST.NAME}'
              priority: WARNING
        - uuid: 8cf7c02cf20e461e8cd2ece183a3549e
          name: 'Processor load (5 min average per core)'
          key: 'system.cpu.load[percpu,avg5]'
          history: 1w
          value_type: FLOAT
          description: 'The processor load is calculated as system CPU load divided by number of CPU cores.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: a8aa7983c18f4788855ff8cd9ec4c926
          name: 'Processor load (15 min average per core)'
          key: 'system.cpu.load[percpu,avg15]'
          history: 1w
          value_type: FLOAT
          description: 'The processor load is calculated as system CPU load divided by number of CPU cores.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 4e9dc5be89a04b53a3c51bfa757f24c1
          name: 'Context switches per second'
          key: system.cpu.switches
          history: 1w
          units: sps
          preprocessing:
            - type: CHANGE_PER_SECOND
              parameters:
                - ''
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 3f2ae412e35e4e77913c42bcb4ffbde2
          name: 'CPU $2 time'
          key: 'system.cpu.util[,idle]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The time the CPU has spent doing nothing.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: e2e95d5be1084c9c86543d4096047b03
          name: 'CPU $2 time'
          key: 'system.cpu.util[,interrupt]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The amount of time the CPU has been servicing hardware interrupts.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 46c5c8d4c6a54730a2e2f787a20c5b96
          name: 'CPU $2 time'
          key: 'system.cpu.util[,iowait]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'Amount of time the CPU has been waiting for I/O to complete.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
          triggers:
            - uuid: ec27ff72fd4d43c48aa062bf086328a5
              expression: 'avg(/Template OS Linux/system.cpu.util[,iowait],5m)>20'
              name: 'Disk I/O is overloaded on {HOST.NAME}'
              priority: WARNING
              description: 'OS spends significant time waiting for I/O (input/output) operations. It could be indicator of performance issues with storage system.'
        - uuid: 33c3075263ca4eff8ca6e2482e109986
          name: 'CPU $2 time'
          key: 'system.cpu.util[,nice]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The time the CPU has spent running users'' processes that have been niced.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 685a286f36b84e1d835f660b045b9798
          name: 'CPU $2 time'
          key: 'system.cpu.util[,softirq]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The amount of time the CPU has been servicing software interrupts.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: fe3b012c9c0c4a948053ab21f8c9eea0
          name: 'CPU $2 time'
          key: 'system.cpu.util[,steal]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The amount of CPU ''stolen'' from this virtual machine by the hypervisor for other tasks (such as running another virtual machine).'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 1128787dc4fa4b97af14f048f00c373e
          name: 'CPU $2 time'
          key: 'system.cpu.util[,system]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The time the CPU has spent running the kernel and its processes.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: c8122476f16649ef950a23fe9f6c1e38
          name: 'CPU $2 time'
          key: 'system.cpu.util[,user]'
          history: 1w
          value_type: FLOAT
          units: '%'
          description: 'The time the CPU has spent running users'' processes that are not niced.'
          request_method: POST
          tags:
            - tag: Application
              value: CPU
            - tag: Application
              value: Performance
        - uuid: 9e656b572c3840fc9289df61b8011bd6
          name: 'Host name'
          key: system.hostname
          delay: 1h
          history: 1w
          trends: '0'
          value_type: CHAR
          description: 'System host name.'
          inventory_link: NAME
          request_method: POST
          tags:
            - tag: Application
              value: General
            - tag: Application
              value: OS
          triggers:
            - uuid: 5f1c31b67601425c90fb98f4992317d7
              expression: '(last(/Template OS Linux/system.hostname,#1)<>last(/Template OS Linux/system.hostname,#2))>0'
              name: 'Hostname was changed on {HOST.NAME}'
              priority: INFO
        - uuid: b8a5f879099f4f33adc12e83097bf2c9
          name: 'Host local time'
          key: system.localtime
          history: 1w
          units: unixtime
          request_method: POST
          tags:
            - tag: Application
              value: General
            - tag: Application
              value: OS
        - uuid: 25164c261b274ea786dd87524196929b
          name: 'Free swap space'
          key: 'system.swap.size[,free]'
          history: 1w
          units: B
          request_method: POST
          tags:
            - tag: Application
              value: Memory
        - uuid: c42386b5947247e3a73bcdbffda72b30
          name: 'Free swap space in %'
          key: 'system.swap.size[,pfree]'
          history: 1w
          value_type: FLOAT
          units: '%'
          request_method: POST
          tags:
            - tag: Application
              value: Memory
          triggers:
            - uuid: 8895f11bcf214f69aa2b9a86f732a182
              expression: 'last(/Template OS Linux/system.swap.size[,pfree])<50'
              name: 'Lack of free swap space on {HOST.NAME}'
              priority: WARNING
              description: 'It probably means that the systems requires more physical memory.'
        - uuid: 4801328922504974a936f6585a0c696c
          name: 'Total swap space'
          key: 'system.swap.size[,total]'
          delay: 1h
          history: 1w
          units: B
          request_method: POST
          tags:
            - tag: Application
              value: Memory
        - uuid: e87dcaa3544e453dbc78a762627fe974
          name: 'System information'
          key: system.uname
          delay: 1h
          history: 1w
          trends: '0'
          value_type: CHAR
          description: 'The information as normally returned by ''uname -a''.'
          inventory_link: OS
          request_method: POST
          tags:
            - tag: Application
              value: General
            - tag: Application
              value: OS
          triggers:
            - uuid: fa85891b0e6947f58e42dae3d212234e
              expression: '(last(/Template OS Linux/system.uname,#1)<>last(/Template OS Linux/system.uname,#2))>0'
              name: 'Host information was changed on {HOST.NAME}'
              priority: INFO
        - uuid: 22214fa5dd054ff2b2d059693d454cb0
          name: 'System uptime'
          key: system.uptime
          delay: 10m
          history: 1w
          units: uptime
          request_method: POST
          tags:
            - tag: Application
              value: General
            - tag: Application
              value: OS
          triggers:
            - uuid: 517678f9284c43429c67bd81d5090c54
              expression: 'change(/Template OS Linux/system.uptime)<0'
              name: '{HOST.NAME} has just been restarted'
              priority: INFO
        - uuid: 455f53504e424fd08c91b1c64354c1d1
          name: 'Number of logged in users'
          key: system.users.num
          history: 1w
          description: 'Number of users who are currently logged in.'
          request_method: POST
          tags:
            - tag: Application
              value: OS
            - tag: Application
              value: Security
        - uuid: f37e2658e2764108aea57bf3277618d3
          name: 'Checksum of $1'
          key: 'vfs.file.cksum[/etc/passwd]'
          delay: 1h
          history: 1w
          request_method: POST
          tags:
            - tag: Application
              value: Security
          triggers:
            - uuid: abc4e5489e8e4fad83868d5eab177963
              expression: '(last(/Template OS Linux/vfs.file.cksum[/etc/passwd],#1)<>last(/Template OS Linux/vfs.file.cksum[/etc/passwd],#2))>0'
              name: '/etc/passwd has been changed on {HOST.NAME}'
              priority: WARNING
        - uuid: 21e695f785f9449696dfd4c0266866c2
          name: 'Available memory'
          key: 'vm.memory.size[available]'
          history: 1w
          units: B
          description: 'Available memory is defined as free+cached+buffers memory.'
          request_method: POST
          tags:
            - tag: Application
              value: Memory
          triggers:
            - uuid: 3dacd2129863462b843b032902ef85b3
              expression: 'last(/Template OS Linux/vm.memory.size[available])<20M'
              name: 'Lack of available memory on server {HOST.NAME}'
              priority: AVERAGE
        - uuid: 29d122c53a1c471e9b5e7f083cd94a93
          name: 'Total memory'
          key: 'vm.memory.size[total]'
          delay: 1h
          history: 1w
          units: B
          request_method: POST
          tags:
            - tag: Application
              value: Memory
      discovery_rules:
        - uuid: b61ebb5701344b7c84d2b86217e954bd
          name: 'Network interface discovery'
          key: net.if.discovery
          delay: 1h
          filter:
            conditions:
              - macro: '{#IFNAME}'
                value: '@Network interfaces for discovery'
                formulaid: A
          description: 'Discovery of network interfaces as defined in global regular expression "Network interfaces for discovery".'
          item_prototypes:
            - uuid: 9dfcdcb258004c769b9c1d730f5552ac
              name: 'Incoming network traffic on $1'
              key: 'net.if.in[{#IFNAME}]'
              history: 1w
              units: bps
              preprocessing:
                - type: CHANGE_PER_SECOND
                  parameters:
                    - ''
                - type: MULTIPLIER
                  parameters:
                    - '8'
              request_method: POST
              tags:
                - tag: Application
                  value: 'Network interfaces'
            - uuid: 0c3dd4e928de40eea133d2e376961f9b
              name: 'Outgoing network traffic on $1'
              key: 'net.if.out[{#IFNAME}]'
              history: 1w
              units: bps
              preprocessing:
                - type: CHANGE_PER_SECOND
                  parameters:
                    - ''
                - type: MULTIPLIER
                  parameters:
                    - '8'
              request_method: POST
              tags:
                - tag: Application
                  value: 'Network interfaces'
          graph_prototypes:
            - uuid: aa9f75260fc74786aed342426869c276
              name: 'Network traffic on {#IFNAME}'
              ymin_type_1: FIXED
              graph_items:
                - drawtype: GRADIENT_LINE
                  color: 00AA00
                  item:
                    host: 'Template OS Linux'
                    key: 'net.if.in[{#IFNAME}]'
                - sortorder: '1'
                  drawtype: GRADIENT_LINE
                  color: 3333FF
                  item:
                    host: 'Template OS Linux'
                    key: 'net.if.out[{#IFNAME}]'
          request_method: POST
        - uuid: d99e64a41e6b4e57bb9126f761e235fe
          name: 'Mounted filesystem discovery'
          key: vfs.fs.discovery
          delay: 1h
          filter:
            conditions:
              - macro: '{#FSTYPE}'
                value: '@File systems for discovery'
                formulaid: A
          description: 'Discovery of file systems of different types as defined in global regular expression "File systems for discovery".'
          item_prototypes:
            - uuid: 3fd5f0e4403f411986ed98dc911f9f5d
              name: 'Free inodes on $1 (percentage)'
              key: 'vfs.fs.inode[{#FSNAME},pfree]'
              history: 1w
              value_type: FLOAT
              units: '%'
              request_method: POST
              tags:
                - tag: Application
                  value: Filesystems
              trigger_prototypes:
                - uuid: d364415d4f754401b8413d0971f06100
                  expression: 'last(/Template OS Linux/vfs.fs.inode[{#FSNAME},pfree])<20'
                  name: 'Free inodes is less than 20% on volume {#FSNAME}'
                  priority: WARNING
            - uuid: 8d0f848e29ef482c9ff4329fbbe47dab
              name: 'Free disk space on $1'
              key: 'vfs.fs.size[{#FSNAME},free]'
              history: 1w
              units: B
              request_method: POST
              tags:
                - tag: Application
                  value: Filesystems
            - uuid: 959519ac683e49579cd63bdbd22b2748
              name: 'Free disk space on $1 (percentage)'
              key: 'vfs.fs.size[{#FSNAME},pfree]'
              history: 1w
              value_type: FLOAT
              units: '%'
              request_method: POST
              tags:
                - tag: Application
                  value: Filesystems
              trigger_prototypes:
                - uuid: c39ed468d32a48f1a87e40183e9570f4
                  expression: 'last(/Template OS Linux/vfs.fs.size[{#FSNAME},pfree])<20'
                  name: 'Free disk space is less than 20% on volume {#FSNAME}'
                  priority: WARNING
            - uuid: 244717e2a2814e7b9ef511e15874678a
              name: 'Total disk space on $1'
              key: 'vfs.fs.size[{#FSNAME},total]'
              delay: 1h
              history: 1w
              units: B
              request_method: POST
              tags:
                - tag: Application
                  value: Filesystems
            - uuid: 0f1c38fb55b74deabae4db657534be2b
              name: 'Used disk space on $1'
              key: 'vfs.fs.size[{#FSNAME},used]'
              history: 1w
              units: B
              request_method: POST
              tags:
                - tag: Application
                  value: Filesystems
          graph_prototypes:
            - uuid: b15c43ed55564882aff4432bcea20d07
              name: 'Disk space usage {#FSNAME}'
              width: '600'
              height: '340'
              show_work_period: 'NO'
              show_triggers: 'NO'
              type: PIE
              show_3d: 'YES'
              graph_items:
                - color: C80000
                  type: GRAPH_SUM
                  item:
                    host: 'Template OS Linux'
                    key: 'vfs.fs.size[{#FSNAME},total]'
                - sortorder: '1'
                  color: 00C800
                  item:
                    host: 'Template OS Linux'
                    key: 'vfs.fs.size[{#FSNAME},free]'
          request_method: POST
      dashboards:
        - uuid: facf223bf2dd4fcaa11d3cdacdcb032a
          name: 'System performance'
          pages:
            - widgets:
                - type: graph
                  width: '12'
                  height: '5'
                  fields:
                    - type: GRAPH
                      name: graphid
                      value:
                        host: 'Template OS Linux'
                        name: 'CPU load'
                - type: graph
                  x: '12'
                  width: '12'
                  height: '5'
                  fields:
                    - type: GRAPH
                      name: graphid
                      value:
                        host: 'Template OS Linux'
                        name: 'CPU utilization'
                - type: graph
                  'y': '5'
                  width: '12'
                  height: '7'
                  fields:
                    - type: GRAPH
                      name: graphid
                      value:
                        host: 'Template OS Linux'
                        name: 'Memory usage'
                - type: graph
                  x: '12'
                  'y': '5'
                  width: '12'
                  height: '7'
                  fields:
                    - type: GRAPH
                      name: graphid
                      value:
                        host: 'Template OS Linux'
                        name: 'Swap usage'
                - type: graph
                  'y': '12'
                  width: '12'
                  height: '5'
                  fields:
                    - type: INTEGER
                      name: source_type
                      value: '1'
                    - type: ITEM
                      name: itemid
                      value:
                        host: 'Template OS Linux'
                        key: 'proc.num[]'
                - type: graph
                  x: '12'
                  'y': '12'
                  width: '12'
                  height: '5'
                  fields:
                    - type: INTEGER
                      name: source_type
                      value: '1'
                    - type: ITEM
                      name: itemid
                      value:
                        host: 'Template OS Linux'
                        key: 'proc.num[,,run]'
  graphs:
    - uuid: b3df089f2510423499ebf9233b1ddb38
      name: 'CPU jumps'
      graph_items:
        - color: '009900'
          item:
            host: 'Template OS Linux'
            key: system.cpu.switches
        - sortorder: '1'
          color: '000099'
          item:
            host: 'Template OS Linux'
            key: system.cpu.intr
    - uuid: ddbadd2946274c3abb1da2973b847a10
      name: 'CPU load'
      ymin_type_1: FIXED
      graph_items:
        - color: '009900'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.load[percpu,avg1]'
        - sortorder: '1'
          color: '000099'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.load[percpu,avg5]'
        - sortorder: '2'
          color: '990000'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.load[percpu,avg15]'
    - uuid: 382b0f47048b45c3a81c2b16e014e551
      name: 'CPU utilization'
      show_triggers: 'NO'
      type: STACKED
      ymin_type_1: FIXED
      ymax_type_1: FIXED
      graph_items:
        - drawtype: FILLED_REGION
          color: FF5555
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,steal]'
        - sortorder: '1'
          drawtype: FILLED_REGION
          color: 55FF55
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,softirq]'
        - sortorder: '2'
          drawtype: FILLED_REGION
          color: '009999'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,interrupt]'
        - sortorder: '3'
          drawtype: FILLED_REGION
          color: '990099'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,nice]'
        - sortorder: '4'
          drawtype: FILLED_REGION
          color: '999900'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,iowait]'
        - sortorder: '5'
          drawtype: FILLED_REGION
          color: '990000'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,system]'
        - sortorder: '6'
          drawtype: FILLED_REGION
          color: '000099'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,user]'
        - sortorder: '7'
          drawtype: FILLED_REGION
          color: '009900'
          item:
            host: 'Template OS Linux'
            key: 'system.cpu.util[,idle]'
    - uuid: 63962d90e11e4ed0992dbc6aed212cde
      name: 'Memory usage'
      ymin_type_1: FIXED
      ymax_type_1: ITEM
      ymax_item_1:
        host: 'Template OS Linux'
        key: 'vm.memory.size[total]'
      graph_items:
        - drawtype: GRADIENT_LINE
          color: 00C800
          item:
            host: 'Template OS Linux'
            key: 'vm.memory.size[available]'
    - uuid: 229091297b684721b676add922834065
      name: 'Swap usage'
      width: '600'
      height: '340'
      show_work_period: 'NO'
      show_triggers: 'NO'
      type: PIE
      show_3d: 'YES'
      graph_items:
        - color: AA0000
          type: GRAPH_SUM
          item:
            host: 'Template OS Linux'
            key: 'system.swap.size[,total]'
        - sortorder: '1'
          color: 00AA00
          item:
            host: 'Template OS Linux'
            key: 'system.swap.size[,free]'
